numpy == 1.23.2
pandas == 2.1.0
sqlalchemy == 2.0.20
lxml == 4.9.3
six == 1.9
html5lib == 1.1
BeautifulSoup4 == 2.4.1
xlrd == 2.0.1
openpyxl == 3.1.2
matplotlib == 3.7.2
scipy == 1.11.2
seaborn == 0.12.2
cufflinks == 0.17.3
plotly == 5.16.1
chart-studio == 1.1.0

